import React from 'react';

import ProjectItem from "./ProjectItem/ProjectItem";

import './ProjectPage.css'

const ProjectPage = () => {

    return (
        <div className="wrapperProject" >

            <div className="container">

                <h1>Our <span>Project</span></h1>

                <div className="projectsList">

                    <ProjectItem/>

                    <ProjectItem/>

                    <ProjectItem/>

                </div>

            </div>

        </div>
    );

};

export default ProjectPage;