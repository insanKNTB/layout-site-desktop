import React from 'react';

import photoProject from '../../../assets/photo/Project/project.png'

import './ProjectItem.css'

const ProjectItem = () => {
    return (

        <div className="projectItem">

            <div className="photoProjectItem">
                <img src={photoProject} alt=""/>
            </div>

            <div className="descriptionProjectItem">

                <h4>Sample Project</h4>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <div className="btnBlock">
                    <button>View More</button>
                </div>

            </div>

        </div>

    );
};

export default ProjectItem;