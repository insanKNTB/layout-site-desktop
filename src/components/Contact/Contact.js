import React from 'react';

import map from '../../assets/photo/map.png'

import './Contact.css'

const Contact = () => {
    return (

        <div className="wrapperContactPage" >

            <div className="container">

                <div className="contact">

                    <div className="contactText">

                        <h1>Project <span>Lorum</span></h1>

                        <div className="companyInformation">
                            <h4>Company Name:</h4>
                            <p>1234 Sample Street Austin Texas 76401</p>
                        </div>

                        <div className="companyInformation">
                            <h4>Phone:</h4>
                            <p>512.333.2222</p>
                        </div>

                        <div className="companyInformation">
                            <h4>E-mail</h4>
                            <p>sampleemail@gmail.com</p>
                        </div>

                        <button>CONTACT US</button>

                    </div>

                    <div className="contactMap">
                        <img src={map} alt=""/>
                    </div>

                </div>

            </div>

        </div>

    );
};

export default Contact;