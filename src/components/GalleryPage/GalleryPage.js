import React from 'react';

import GalleryItem from "./GalleryItem/GalleryItem";

import './GalleryPage.css'

const GalleryPage = () => {
    return (
        <div className="wrapperGallery" >

            <div className="container">

                <h1>Photo <span>Gallery</span></h1>

                <div className="gallery">

                    <GalleryItem/>

                </div>

            </div>

        </div>
    );
};

export default GalleryPage;