import React, {Fragment} from 'react';

import GalleryPhoto from '../../../assets/photo/Gallery/1.png'

const GalleryItem = () => {

    return (

        <Fragment>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

            <div className="galleryItem" >
                <img src={GalleryPhoto} alt=""/>
            </div>

        </Fragment>

    );

};

export default GalleryItem;