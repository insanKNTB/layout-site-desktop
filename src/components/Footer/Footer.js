import React from 'react';
import {Link} from "react-router-dom";

import './Footer.css'

import logoWhite from '../../assets/photo/fvcWhite.svg'
import facebook from '../../assets/photo/Footer/social/facebook.png'
import twitter from '../../assets/photo/Footer/social/twitter.png'
import linkedIn from '../../assets/photo/Footer/social/Linked In.png'
import pinterest from '../../assets/photo/Footer/social/pininterest.png'

const Footer = props => {
    return (
        <div className="wrapperFooter">

            <div className="container">

                <div className="footer">

                <div className="logoFooter">
                    <img src={logoWhite} alt=""/>
                </div>

                <div className="footerNavigation">

                    <p>Information</p>

                    <ul>
                        <li>
                            <Link
                                to="/"
                                onClick={() => props.setNewPage(0)}
                            >
                                Main
                            </Link>
                        </li>

                        <li>
                            <Link
                                to="/gallery"
                                onClick={() => props.setNewPage(1)}
                            >
                                Gallery
                            </Link>
                        </li>

                        <li>
                            <Link
                                to="/project"
                                onClick={() => props.setNewPage(2)}
                            >
                                Projects
                            </Link>
                        </li>

                        <li>
                            <Link
                                to="/certifications"
                                onClick={() => props.setNewPage(3)}
                            >
                                Certifications
                            </Link>
                        </li>

                        <li>
                            <Link
                                to="/contacts"
                                onClick={() => props.setNewPage(4)}
                            >
                                Contacts
                            </Link>
                        </li>

                    </ul>

                </div>

                <div className="footerContact">

                    <p>Contacts</p>

                    <ul>
                        <li className="addressFooter" >1234 Sample Street Austin Texas 78704</li>
                        <li className="numberFooter" >512.333.2222</li>
                        <li className="emailFooter" >sampleemail@gmail.com</li>
                    </ul>

                </div>

                <div className="footerSocialMedia">

                    <p>Social Media</p>

                    <ul>

                        <li>
                            <img src={facebook} alt=""/>
                        </li>

                        <li>
                            <img src={twitter} alt=""/>
                        </li>

                        <li>
                            <img src={linkedIn} alt=""/>
                        </li>

                        <li>
                            <img src={pinterest} alt=""/>
                        </li>

                    </ul>

                </div>

            </div>

            </div>

            <h3 className="textEndSite">© 2021 All Rights Reserved</h3>

        </div>
    );
};

export default Footer;