import {Link} from "react-router-dom";

import logo from '../../assets/photo/fvc.svg'

import './Header.css'

const Header = props => {

    return (
        <div className="wrapperHeader">

            <div className="container">

                <div className="header">

                    <div className="logoCompany">

                        <Link
                            to="/"
                            onClick={() => props.setNewPage(0)}
                        >
                            <img src={logo} alt=""/>
                        </Link>

                    </div>

                    <ul className="navigationHeader">

                        <li>

                            <Link
                                to="/"
                                className={0 === props.nowPage ? 'nowPage' : 'navigationItem'}
                                onClick={() => props.setNewPage(0)}
                            >
                                Main page
                            </Link>

                        </li>

                        <li>

                            <Link
                                to="/gallery"
                                className={1 === props.nowPage ? 'nowPage' : 'navigationItem'}
                                onClick={() => props.setNewPage(1)}
                            >
                                Gallery
                            </Link>

                        </li>

                        <li>

                            <Link
                                to="/project"
                                className={2 === props.nowPage ? 'nowPage' : 'navigationItem'}
                                onClick={() => props.setNewPage(2)}
                            >
                                Project
                            </Link>

                        </li>

                        <li>

                            <Link
                                to="/certifications"
                                className={3 === props.nowPage ? 'nowPage' : 'navigationItem'}
                                onClick={() => props.setNewPage(3)}
                            >
                                CERTIFICATIONS
                            </Link>

                        </li>

                        <li>

                            <Link
                                to="/contacts"
                                className={4 === props.nowPage ? 'nowPage' : 'navigationItem'}
                                onClick={() => props.setNewPage(4)}
                            >
                                Contacts
                            </Link>

                        </li>
                    </ul>

                </div>

            </div>

        </div>
    );
};

export default Header;