import React from 'react';
import {Link} from "react-router-dom";

import firstPhoto from '../../../assets/photo/ProjectPhoto/1.png'
import secondPhoto from '../../../assets/photo/ProjectPhoto/2.png'
import therePhoto from '../../../assets/photo/ProjectPhoto/3.png'
import forthPhoto from '../../../assets/photo/ProjectPhoto/4.png'
import fifthPhoto from '../../../assets/photo/ProjectPhoto/5.png'

import './ProjectainPage.css'

const ProjectMainPage = () => {

    return (
        <div className="wrapperProjectMainPage" >
            <h3>Our Projects</h3>

            <div className="projectMainPage">

                <div className="firstRow">

                    <div className="photoFirst projectPhoto">
                        <img src={firstPhoto} alt=""/>
                    </div>

                    <div className="photoSecond projectPhoto">
                        <img src={secondPhoto} alt=""/>
                    </div>

                </div>

                <div className="secondRow">

                    <div className="photoThere projectPhoto">
                        <img src={therePhoto} alt=""/>
                    </div>

                    <div className="photoForth projectPhoto">
                        <img src={forthPhoto} alt=""/>
                    </div>

                    <div className="photoFifth projectPhoto">
                        <img src={fifthPhoto} alt=""/>
                    </div>

                </div>

            </div>

            <div className="btnProject">

                <Link to="/project" >
                    <button>All Projects</button>
                </Link>

            </div>

        </div>
    );

};

export default ProjectMainPage;