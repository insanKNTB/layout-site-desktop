import React from 'react';
import {Link} from "react-router-dom";

import firstPhoto from '../../../assets/photo/AboutUs/1.png'
import secondPhoto from '../../../assets/photo/AboutUs/2.png'
import therePhoto from '../../../assets/photo/AboutUs/3.png'

import './AboutUs.css'

const AboutUs = () => {
    return (

        <div className="wrapperAboutUs">

            <div className="photoAboutCompany">

                <div className="firstColumnPhoto">

                    <img src={firstPhoto} className="firstPhoto" alt=""/>

                    <img src={secondPhoto} className="secondPhoto" alt=""/>

                </div>

                <div className="secondColumnPhoto">

                    <img src={therePhoto} className="therePhoto" alt=""/>

                </div>

            </div>

            <div className="aboutTextCompany">

                <h3>About</h3>

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                <Link to="/contacts" >
                    <button>Read More</button>
                </Link>

            </div>

        </div>

    );
};

export default AboutUs;