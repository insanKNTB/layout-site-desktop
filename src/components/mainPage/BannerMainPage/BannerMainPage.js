import React from 'react';
import {Link} from "react-router-dom";

import Banner from "../../../assets/photo/banner.png";

const BannerMainPage = () => {
    return (
        <div className="bannerMainPage">

            <div className="aboutTextBanner">

                <h1>Project <span>Lorum</span></h1>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum, quaerat.
                </p>

            </div>

            <div className="photoBanner">

                <img src={Banner} alt=""/>

                <Link to="/project" >
                    <button>VIEW PROJECT</button>
                </Link>

            </div>

        </div>
    );
};

export default BannerMainPage;