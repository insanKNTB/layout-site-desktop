import React from 'react';

import BannerMainPage from "./BannerMainPage/BannerMainPage";
import AboutUs from "./AboutUsMainPage/AboutUs";
import MissionMainPage from "./MissionMainPage/missionMainPage";
import ProjectMainPage from "./ProjectMainPage/ProjectMainPage";
import ContactUs from "./ComtactUs/ContactUs";

import './BannerMainPage/BannerMainPage.css'

const MainPage = () => {

    return (
        <div className="mainPage">

            <div className="container">

                <BannerMainPage/>

                <AboutUs/>

                <MissionMainPage/>

                <ProjectMainPage/>

                <ContactUs/>

            </div>

        </div>
    );

};

export default MainPage;