import React from 'react';

import './missionMainPage.css'

const MissionMainPage = () => {

    return (
        <div className="missionPage" >

            <h3>Main Focus/Mission Statement</h3>

            <div className="missionList">

                <div className="firstListMission">

                    <span>1</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed efficitur, lectus et facilisis placerat.</p>

                </div>

                <div className="secondListMission">

                    <span>2</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed efficitur, lectus et facilisis placerat, magna mauris porttitor tortor, a auctor est felis ut nisl.</p>

                </div>

            </div>

        </div>
    );

};

export default MissionMainPage;