import React from 'react';

import contactUs from '../../../assets/photo/contactUsPhoto.png'
import './ContactUs.css'

const ContactUs = () => {
    return (

        <div className="wrapperContact" >

            <h1>Contact Us</h1>

            <div className="contact">

                <form action="" className="formContact" >

                    <input type="text" className="inputText" placeholder="Name" />
                    <input type="number" className="inputText" placeholder="Phone Number" />
                    <input type="text" className="inputText" placeholder="E-mail" />
                    <input type="text" className="inputText" placeholder="Interested In" />

                    <textarea cols="30" rows="10" className="inputText" placeholder="Massage" >
                    </textarea>

                </form>

                <div className="contactUsPhoto">
                    <img src={contactUs} alt=""/>
                </div>

            </div>

            <div className="btnContact">
                <button>
                    Send Email
                </button>
            </div>

        </div>

    );
};

export default ContactUs;