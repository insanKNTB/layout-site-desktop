import React, {Fragment} from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

import MainPage from "../../components/mainPage/MainPage";
import Header from "../../components/Header/Header";
import Contact from "../../components/Contact/Contact";
import GalleryPage from "../../components/GalleryPage/GalleryPage";
import ProjectPage from "../../components/ProjectPage/ProjectPage";
import Certifications from "../../components/CertificationsPage/Certifications";
import Footer from "../../components/Footer/Footer";

import './App.css';

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state={
            nowViewPage: 0
        }
    }

    setNewPage = (index) => this.setState({nowViewPage: index})

    render() {

        return (
            <Fragment>

                <Router>

                    <Header
                        nowPage={this.state.nowViewPage}
                        setNewPage={this.setNewPage}
                    />

                    <Switch>

                        <Route path="/" exact component={MainPage}/>
                        <Route path="/gallery" component={GalleryPage}/>
                        <Route path="/project" component={ProjectPage}/>
                        <Route path="/certifications" component={Certifications}/>
                        <Route path="/contacts" component={Contact}/>

                    </Switch>

                    <Footer
                        setNewPage={this.setNewPage}
                    />

                </Router>

            </Fragment>
        )

    }

}


export default App;
